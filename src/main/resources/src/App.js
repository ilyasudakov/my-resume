import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.scss';
import './variables.scss';
import Header from './components/Header/Header.jsx';
import WorkExperience from './components/WorkExperience/WorkExperience.jsx';
import Education from './components/Education/Education.jsx';
import Skills from './components/Skills/Skills.jsx';
import Projects from './components/Projects/Projects.jsx';

class App extends React.Component {
  componentDidMount() {

  }

  render() {
    return (
      <div className="resume_page">
        <Header />
        <div className="resume_page__content">
          {/* <WorkExperience /> */}
          <Education />
          <Skills />
          <WorkExperience />
          <Projects />
        </div>
      </div>
    );
  }
}

export default App;