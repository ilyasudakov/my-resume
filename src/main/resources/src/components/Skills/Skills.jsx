import React from 'react';
import './Skills.scss';

const Skills = () => {
    return (
        <div className="skills">
            <div className="skills__title">Навыки</div>
            <div className="skills__item">
                <div className="skills__name">Технологии</div>
                <div className="skills__list">
                    <div className="skills__bubble">HTML</div>
                    <div className="skills__bubble">JavaScript</div>
                    <div className="skills__bubble">JavaScript ES6</div>
                    <div className="skills__bubble">CSS</div>
                    <div className="skills__bubble">Методология БЭМ</div>
                    <div className="skills__bubble">C++</div>
                </div>
            </div>
            <div className="skills__item">
                <div className="skills__name">Библиотеки / Фреймворки</div>
                <div className="skills__list">
                    <div className="skills__bubble">React</div>
                    <div className="skills__bubble">SASS</div>
                    <div className="skills__bubble">Express</div>
                    <div className="skills__bubble">Babel</div>
                    <div className="skills__bubble">Webpack</div>
                    <div className="skills__bubble">React Motion</div>
                </div>
            </div>
            <div className="skills__item">
                <div className="skills__name">Инструменты</div>
                <div className="skills__list">
                    <div className="skills__bubble">Git(Bitbucket, Github)</div>
                    <div className="skills__bubble">Trello</div>
                    <div className="skills__bubble">Figma</div>
                    <div className="skills__bubble">Photoshop</div>
                    <div className="skills__bubble">Visual Studio Code</div>
                    <div className="skills__bubble">IntelliJ IDEA</div>
                    <div className="skills__bubble">Talend API Tester(аналог Postman)</div>
                </div>
            </div>
        </div>
    );
};

export default Skills;