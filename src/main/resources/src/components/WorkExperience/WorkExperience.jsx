import React from 'react';
import './WorkExperience.scss';

const WorkExperience = () => {
    return (
        <div className="work-experience">
            <div className="work-experience__title">Опыт работы</div>
            <div className="work-experience__job">
                <div className="work-experience__position">Фронтенд разработчик (React)</div>
                <div className="work-experience__company">Osfix</div>
                <div className="work-experience__description">Cоздание фронтенда для CRM-системы промышленного производства</div>
                <div className="work-experience__stats">
                    <div className="work-experience__period">10/2019 - 01/2020</div>
                    <a className="work-experience__link" href="https://imgur.com/a/ZaQZfXZ" target="_blank" rel="noopener noreferrer">Скриншоты</a>
                </div>
                <ul className="work-experience__tasks">
                    <li className="work-experience__item">Создание системы ролей для ограничения доступа к отдельным компонентам, для определенных пользователей</li>
                    <li className="work-experience__item">Создание компонента формы, компонента ошибки для полей ввода</li>
                    <li className="work-experience__item">Написание API для взаимодействия с бэкендом</li>
                    <li className="work-experience__item">Авторизация пользователя, работа с refreshToken и accessToken</li>
                    <li className="work-experience__item">Использование lazy-загрузки компонентов для улучшения производительности</li>

                </ul>
            </div>
            <div className="work-experience__job">
                <div className="work-experience__position">Фронтенд разработчик</div>
                <div className="work-experience__company">Фриланс</div>
                <div className="work-experience__description">Верстка обновленного дизайна для сайта, создание админ-панели для управления контентом</div>
                <div className="work-experience__stats">
                    <div className="work-experience__period">07/2019 - 09/2019</div>
                    <a className="work-experience__link" href="https://imgur.com/a/U6ritgK" target="_blank" rel="noopener noreferrer">Скриншоты</a>
                </div>
                <ul className="work-experience__tasks">
                    <li className="work-experience__item">Работа с react-router</li>
                    <li className="work-experience__item">Переход проекта на препроцессор CSS - SASS</li>
                    <li className="work-experience__item">Верстка дизайна, предоставленного заказчиком</li>
                    <li className="work-experience__item">Работа с localStorage</li>
                    <li className="work-experience__item">Начал использовать методологию БЭМ</li>
                </ul>
            </div>
        </div>
    );
};

export default WorkExperience;