import React from 'react';
import MailPNG from '../../../../../../assets/header/email.png';
import PhonePNG from '../../../../../../assets/header/phone.png';
import BitBucketPNG from '../../../../../../assets/header/bitbucket.png';
import HHPNG from '../../../../../../assets/header/hh.png';
import './Header.scss';

const Header = () => {
    return (
        <div className="header">
            <div className="header__bio">
                <div className="header__fullname">Судаков Илья Сергеевич</div>
                <div className="header__position">Фронтенд разработчик</div>
                <div className="header__about-me">
                    Работал над созданием фронтенда для CRM-системы промышленного производства(React + SASS, разработка API для связи с бэкендом), 
                    занимаюсь разработкой собственных проектов. Изучаю дизайн и инструменты для его создания(Photoshop, Figma)
                </div>
            </div>
            <div className="header__links"> 
                <a className="header__item" href="mailto:ilyasudakov@inbox.ru" target="_blank" rel="noopener noreferrer">
                    <div className="header__link">ilyasudakov@inbox.ru</div>
                    <img className="header__img" src={MailPNG} alt="" />
                    <div className="header__link header__link--mobile">Почта</div>
                </a>
                {/* <a className="header__item" href="tel:89119731351">
                    <div className="header__link">+7 (911) 973-13-51</div>
                    <img className="header__img" src={PhonePNG} alt="" />
                    <div className="header__link header__link--mobile">Телефон</div>
                </a> */}
                <a className="header__item" href="https://bitbucket.org/ilyasudakov/" target="_blank" rel="noopener noreferrer">
                    <div className="header__link">Bitbucket</div>
                    <img className="header__img" src={BitBucketPNG} alt="" />
                    <div className="header__link header__link--mobile">BitBucket</div>
                </a>
                <a className="header__item" href="https://spb.hh.ru/resume/5ec35f27ff076a2f870039ed1f6c3464485868" target="_blank" rel="noopener noreferrer">
                    <div className="header__link">HeadHunter</div>
                    <img className="header__img" src={HHPNG} alt="" />
                    <div className="header__link header__link--mobile">hh.ru</div>
                </a>
            </div>
        </div>
    );
};

export default Header;