import React from 'react';
import './Education.scss';

const Education = () => {
    return (
        <div className="education">
            <div className="education__title">Образование</div>
            <div className="education__place">
                <div className="education__degree">Бакалавр - Программная инженерия (4 курс)</div>
                <div className="education__university">Санкт-Петербургский Государственный Университет Телекоммуникаций им. Бонч-Бруевича</div>
                <div className="education__stats">
                    <div className="education__period">09/2016 - Настоящее время</div>
                    <div className="education__gpa">GPA 4.51 / 5.00</div>
                </div>
            </div>
        </div>
    );
};

export default Education;