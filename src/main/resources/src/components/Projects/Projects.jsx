import React from 'react';
import "./Projects.scss";

const Projects = () => {
    return (
        <div className="projects">
            <div className="projects__title">Проекты</div>
            <div className="projects__item">
                <div className="projects__name">Advicer</div>
                <div className="projects__description">Сервис менеджмента фидбека пользователей</div>
                <div className="projects__stats">
                    <div className="projects__period">10/2019 - Настоящее время</div>
                    <a className="projects__link" href="https://imgur.com/a/w63gT4n" target="_blank" rel="noopener noreferrer">Скриншоты</a>
                </div>
                <ul className="projects__tasks">
                    <li className="projects__item">Интеграция авторизации, используя Google API</li>
                    <li className="projects__item">Создание системы ролей(Пользователь и Разработчик)</li>
                    <li className="projects__item">Возможность оставлять отзывы на продукцию</li>
                    <li className="projects__item">Создание собственного дизайна с помощью сервиса Figma</li>
                </ul>
            </div>
        </div>
    );
};

export default Projects;